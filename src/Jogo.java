
public class Jogo {

	Dado dado;

	public Jogo(Dado dado) {
		this.dado = dado;
	}

	public void geraJogada(int qtdRodadas) {

		for (int x = 0; x < qtdRodadas; x++) {
			int total = 0;
			for (int i = 0; i < 3; i++) {
				int jogada = dado.jogaDado();
				total += jogada;
				System.out.print(jogada + ", ");
			}
			System.out.print(total + ".");
			System.out.println();
		}
	}

}
