import java.util.Random;

public class Dado {
	
	private int lados;
	
	public Dado(int lados) {
		
		this.lados = lados;
		
	}
	
	public int jogaDado() {
		
		Random lado = new Random();
		
		return lado.nextInt(this.lados) + 1;
	}
	

}
