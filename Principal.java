import java.util.Random;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {

		Scanner qtdLados = new Scanner(System.in);

		System.out.println("Digite a Quantidade de lados do Dado: ");
		
		int lados = qtdLados.nextInt();

		Dado dado = new Dado(lados);
		
		Jogo jogo = new Jogo(dado);
		
		jogo.geraJogada(3);

	}

}
